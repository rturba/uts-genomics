#!/bin/bash

#$ -l h_data=24G,h_rt=96:00:00,highp
#$ -N treemix
#$ -cwd
#$ -m bea
#$ -o /u/home/r/rturba/scripts/run_logs/$JOB_NAME_$JOB_ID.out
#$ -e /u/home/r/rturba/scripts/run_logs/$JOB_NAME_$JOB_ID.err
#$ -M rturba
#$ -t 1-12:1

source /u/local/Modules/default/init/bash

module load anaconda3
conda activate treemix

wd=/u/scratch/r/rturba/stickleback/treemix
filename=stick88_xlSet1a.1_updatedIdsSex_LDpruned_geno0_noSACind

mkdir -p $wd/tree_output/boot10_ATL_noSAC
outdir=$wd/tree_output/boot10_ATL_noSAC

m=${SGE_TASK_ID}

for i in {1..10}; do
treemix -i $wd/${filename}_treemix.frq.gz \
-o $outdir/${filename}_treemix_m${m}_i${i} \
-m $m \
-root ATL \
-bootstrap -k 10 \
-global > $outdir/treemix_m${m}_i${i}.log 2>&1
done
