#! /bin/bash

#$ -cwd
#$ -l h_rt=120:00:00,h_data=20G,highp
#$ -N fastStructure_simple
#$ -o /u/home/r/rturba/scripts/run_logs/$JOB_NAME_$JOB_ID.out
#$ -e /u/home/r/rturba/scripts/run_logs/$JOB_NAME_$JOB_ID.err
#$ -m abe
#$ -M rturba

source /u/local/Modules/default/init/bash

module load anaconda3
conda activate faststructure

wd=/u/scratch/r/rturba/stickleback
plink=$wd/plink_files/stick88_xlSet2a.1_bialSnps_updatedIdsSex_LDpruned

for K in {1,2,3,4,5,6,7,8,9,10}; do
structure.py -K $K \
--input=$plink \
--output=$wd/faststructure/stick88_xlSet2a.1_updatedIdsSex_LDpruned_simple \
--full --seed=100
done
