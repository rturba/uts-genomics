#! /bin/bash

#$ -cwd
#$ -l h_rt=03:00:00,h_data=10G,highp
#$ -N fastStructure_plot
#$ -o /u/home/r/rturba/scripts/run_logs/$JOB_NAME_$JOB_ID.out
#$ -e /u/home/r/rturba/scripts/run_logs/$JOB_NAME_$JOB_ID.err
#$ -m abe
#$ -M rturba

source /u/local/Modules/default/init/bash

module load anaconda3
conda activate faststructure

wd=/u/scratch/r/rturba/stickleback/faststructure
mkdir -p $wd/output_xlSet2a.1_bialSnps/plots_simple

for K in {1,2,3,4,5,6,7,8,9,10}; do
distruct.py -K $K \
--input=$wd/output_xlSet2a.1_bialSnps/stick88_xlSet2a.1_updatedIdsSex_LDpruned_simple \
--output=$wd/output_xlSet2a.1_bialSnps/plots_simple/stick88_xlSet2a.1_updatedIdsSex_LDpruned_simple_K${K}.svg \
--popfile=$wd/stick88_xlSet2a.1_popfile_fastStructure.txt
done
